/**
 * @author: Hópur 7; Elsa Mjöll,Kári Yngva og Rakel Björt
 * @since: 30.10.14
 * Utility klasi sem inniheldur aðferð til að ná í notanda eftir streng en ekki eftir objectID
 */
package is.ekr.whereyouapp;

import android.app.Application;
import android.util.Log;
import android.content.Intent;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;

import java.util.List;

public class WhereYouApp extends Application {

    @Override
    public void onCreate() {
        Parse.enableLocalDatastore(this);
        ParseObject.registerSubclass(Message.class);
        Parse.initialize(this, "DCL17wRsdp5f9FQz4qfnypbqmcsDW6sRkuJYMMQQ", "HxVMoiNpyP2O6y1ICx3D2M7LtNfbQkJxHWrR82Cx");
    }

    /**
     * Aðferð sem leitar að tengiliði á síma eftir notendanafni
     * @param username notenda nafn þess notedanda sem leitað er eftir
     **/
    public static ParseUser getContactByName(String username) {
        ParseUser user = null;
        ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
        userQuery.fromLocalDatastore();
        userQuery.whereEqualTo("username", username);

        try {
            user = userQuery.getFirst();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return user;
    }

    public static boolean logout(){
        ParseUser.logOut();
        return (ParseUser.getCurrentUser() == null);
    }

    /**
     * Aðferð sem bætir innskráðum notanda við installation-hlutinn svo hægt sé að senda viðkomandi push notification
     * @param user sá notandi sem á að bæta við installation-hlutinn
     */
    public static void addUserToCurrentInstallation(ParseUser user) {
        //Add the currents users username to the current installation on order to send him or her push notifications later on
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();

        installation.put("username", user.getUsername());
        installation.saveInBackground();
        System.out.println("WYA: addUserToCurrentInstallation");

        ParsePush.subscribeInBackground(user.getUsername(), new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the username channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });
    }
}