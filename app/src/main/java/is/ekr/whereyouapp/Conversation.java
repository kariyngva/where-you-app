/**
 * @author: Hópur 7; Elsa Mjöll,Kári Yngva og Rakel Björt
 * @since: 12.11.14
 * Klasinn sem birtir samtal milli tveggja notanda
 */
package is.ekr.whereyouapp;

import android.app.Activity;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.parse.ParseUser;

import java.util.List;


public class Conversation extends Activity {

    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private EditText send_message;
    private Button btnSend;
    public Messages messages;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        setContentView(R.layout.activity_conversation);
        Bundle extras = getIntent().getExtras();

        btnSend = (Button) findViewById(R.id.btnSend);
        listView = (ListView) findViewById(R.id.listView1);
        chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(), R.layout.activity_chat_singlemessage);
        listView.setAdapter(chatArrayAdapter);

        send_message = (EditText) findViewById(R.id.send_message);
        send_message.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    return sendChatMessage();
                }
                return false;
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                sendChatMessage();
            }
        });

        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);

        //to scroll the list view to bottom on data change
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });

        //After all init we check if we have a receiver and then loop over previous messages
        if (extras != null) {
            ParseUser receiver = WhereYouApp.getContactByName(extras.getString("username"));

            //If for some reason getContactByName returns null we don't want to create a new message object.
            if ( receiver != null ) {

                //Create new Messages object and loop over and display messages
                messages = new Messages(receiver);
                List<Message> messageList = messages.getMessageList();
                for( Message message : messageList )
                {
                    //Create a chat bubble and display on the senders screen
                    ChatBubble bubble = new ChatBubble(message.senderIsCurrentUser(), message.getMessageBody());
                    chatArrayAdapter.add(bubble);
                }
            }
        }
    }

    /**
     * Aðferð sem birtir skilaboð og sendir skilaboð sem notandi hefur slegið inn og valið að senda.
     */
    private boolean sendChatMessage(){
        String messageBody = send_message.getText().toString();
        boolean side = messages.sendMessage(messageBody).senderIsCurrentUser();
        chatArrayAdapter.add(new ChatBubble(side, messageBody)); //create a chat bubble and display on the senders screen
        send_message.setText("");
        return true;
    }
}