/**
 * @author: Hópur 7; Elsa Mjöll,Kári Yngva og Rakel Björt
 * @since: 30.10.14
 * Klasinn sem birtir tengiliðina í lista sem passar við leitarstrenginn þinn.
 */package is.ekr.whereyouapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.List;


public class SearchResult extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle extras = getIntent().getExtras();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        //Get search term from MessageOverview
        if (extras != null) {
            String searchTerm  = extras.getString ("searchTerm");
            searchForContact(searchTerm);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_sign_out) {
            if(WhereYouApp.logout())
            {
                Intent i = new Intent(getApplicationContext(), LogIn.class);
                startActivity(i);
                Toast toast = Toast.makeText(getApplicationContext(), "You have Logged Out!",
                Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_HORIZONTAL, 0, -60);
                toast.show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Aðferð sem leitar að notanda á parse þjón eftir notendanafni eða símanúmeri
     * @param searchTerm er leitarstrengur sem er annaðhvort símanúmer eða notendanafn
     **/
    public void searchForContact(String searchTerm) {
        ParseQuery<ParseUser> userQuery = ParseUser.getQuery();

        //Check if the search term is a number and handle the search accordingly
        if ( searchTerm.matches("[0-9]+") )
        {
            userQuery.whereEqualTo("phone_number", searchTerm);
        }
        else
        {
            userQuery.whereEqualTo("username", searchTerm);
        }

        userQuery.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> users, ParseException e) {

                if (e == null) {
                    System.out.println(users.size());
                    displayResults(users);

                }
                if (users.size() == 0) {
                    Intent i = new Intent(getApplicationContext(), MessageOverview.class);
                    startActivity(i);
                    Toast toast = Toast.makeText(getApplicationContext(), "No contact found",
                    Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_HORIZONTAL, 0, -60);
                    toast.show();
                }
            }
        });
    }

    /**
     * Aðferð sem birtir lista af notendum
     * @param users er listi af ParseUser
     **/
    public void displayResults(List<ParseUser> users) {
        String userArray[] = new String[users.size()];

        //User list to array of strings containing usernames
        for (int i = 0; i< users.size(); i++){
            userArray[i] = users.get(i).getString("username");
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, userArray);

        ListView listView = (ListView) findViewById(R.id.posts_listview);
        // Assign adapter to ListView
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tw = (TextView) view;
                addContact(tw.getText().toString());
            }
        });
    }

    /**
     * Aðferð sem bætir notanda við sem tengiliði á síma
     * @param username er notendanafn þess notanda sem skal vista
     **/
    public void addContact(String username) {

        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("username", username);
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> users, ParseException e) {
                if (e == null) {
                    //There should only be one user as usernames should be unique
                    ParseUser contact = users.get(0);
                    contact.setACL(new ParseACL(ParseUser.getCurrentUser()));
                    contact.pinInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                //success
                                //Switch to message overview
                                Intent i = new Intent(getApplicationContext(), MessageOverview.class);
                                startActivity(i);

                            } else {
                                //fail
                                Toast toast = Toast.makeText(getApplicationContext(), "There was an error getting contact",
                                Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_HORIZONTAL, 0, -60);
                                toast.show();
                            }
                        }
                    });

                    //Switch to message overview
                    //Tharf kannski ekki heldur að vera hér...
                    Intent i = new Intent(getApplicationContext(), MessageOverview.class);
                    startActivity(i);

                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Contact was not added",
                    Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_HORIZONTAL, 0, -60);
                    toast.show();
                }
            }
        });
    }
}