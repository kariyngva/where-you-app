/**
 * @author: Hópur 7; Elsa Mjöll,Kári Yngva og Rakel Björt
 * @since: 12.11.14
 * Klasi sem heldur utan um talblöðrur (ChatBubble) skilaboða.
 */

package is.ekr.whereyouapp;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ChatArrayAdapter extends ArrayAdapter<ChatBubble> {

    private TextView send_message;
    private List<ChatBubble> chatBubbleList = new ArrayList<ChatBubble>();
    private LinearLayout singleMessageContainer;

    @Override
    public void add(ChatBubble object) {
        chatBubbleList.add(object);
        super.add(object);
    }

    public ChatArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public int getCount() {
        return this.chatBubbleList.size();
    }

    public ChatBubble getItem(int index) {
        return this.chatBubbleList.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.activity_chat_singlemessage, parent, false);
        }
        singleMessageContainer = (LinearLayout) row.findViewById(R.id.singleMessageContainer);
        ChatBubble chatBubbleObj = getItem(position);
        send_message = (TextView) row.findViewById(R.id.singleMessage);
        send_message.setText(chatBubbleObj.message);
        send_message.setBackgroundResource(chatBubbleObj.left ? R.drawable.bubble_a : R.drawable.bubble_b);
        singleMessageContainer.setGravity(chatBubbleObj.left ? Gravity.RIGHT : Gravity.LEFT);
        return row;
    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

}