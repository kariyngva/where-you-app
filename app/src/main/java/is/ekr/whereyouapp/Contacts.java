/**
 * @author: Hópur 7; Elsa Mjöll,Kári Yngva og Rakel Björt
 * @since: 30.10.14
 * Klasinn birtir alla þá tengiliði sem þú ert búinn að bæta við sem tengiliði (MyContacts).
 */
package is.ekr.whereyouapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;


public class Contacts extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        getContacts();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.contacts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_sign_out) {
            if(WhereYouApp.logout())
            {
                Intent i = new Intent(getApplicationContext(), LogIn.class);
                startActivity(i);
                Toast toast = Toast.makeText(getApplicationContext(), "You have Logged Out!",
                Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_HORIZONTAL, 0, -60);
                toast.show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Aðferð sem leitar að öllum tengiliðum á síma.
    **/
    public void getContacts() {
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseUser>() {
            public void done(List<ParseUser> users, ParseException e) {
                if (e == null) {
                    displayContacts(users);
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "You have no contacts",
                    Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_HORIZONTAL, 0, -60);
                    toast.show();
                }
            }
        });
    }

    /**
     * Aðferð sem birtir lista af tengliðum/notendum
     * @param users listi af ParseUser
    **/
    public void displayContacts(List<ParseUser> users) {
        String userArray[] = userListToStringArray(users);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, userArray);

        ListView listView = (ListView) findViewById(R.id.contacts_listview);

        // Assign adapter to ListView
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            //Færum okkur yfir í conversation activity þegar smellt er á contact og sendum notendanafn yfir sem "extra".
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView username = (TextView) view;
                Intent i = new Intent(getApplicationContext(), Conversation.class);
                i.putExtra("username", username.getText().toString());
                startActivity(i);
            }
        });
    }

    /**
     * Aðferð sem skilar strengjafylki af notendanöfnum.
     * @param users listi af ParseUser
     * @return strengjafylki sem inniheldur notendanöfn stakanna í users
     */
    public String[] userListToStringArray(List<ParseUser> users) {
        users.remove(ParseUser.getCurrentUser()); //remove the current user from the list so we don't display him/her as a contact
        String userArray[] = new String[users.size()];

        for (int i = 0; i< users.size(); i++){
            userArray[i] = users.get(i).getString("username");
        }

        return userArray;
    }
}