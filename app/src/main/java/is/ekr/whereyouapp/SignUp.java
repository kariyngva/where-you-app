/**
 * @author: Hópur 7; Elsa Mjöll,Kári Yngva og Rakel Björt
 * @since: 13.10.14
 * Klasinn sem birtir nýskráningarform og nýtir sér parse þjónustuna til að búa til nýjan notenda.
 */
package is.ekr.whereyouapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseUser;
import com.parse.ParseException;
import com.parse.SignUpCallback;


public class SignUp extends Activity {
    public static String usernameString;
    public static String passwordString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_sign_out) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * Aðferð sem býr til nýjan notenda, auðkennir og vistar á parse þjóni
     **/
    public void registerUser(View view) {
        //Get values from the registration form.
        TextView username = (TextView) findViewById(R.id.id_name);
        TextView password = (TextView) findViewById(R.id.password);
        TextView passwordConfirm = (TextView) findViewById(R.id.password2);
        final TextView phone_number = (TextView) findViewById(R.id.phone_number);
        usernameString = username.getText().toString();
        passwordString = password.getText().toString();

        if (phone_number.getText().toString().matches("[0-9]+") && phone_number.length() == 7) {
            if (passwordString.equals(passwordConfirm.getText().toString())) {
                //Create a new Parse user and try to save it.
                ParseUser user = new ParseUser();
                user.setUsername(usernameString);
                user.setPassword(passwordString);
                user.put("phone_number", phone_number.getText().toString());


                user.signUpInBackground(new SignUpCallback() {
                    public void done(ParseException e) {
                        if (e == null) {
                            ParseUser.logInInBackground(usernameString, passwordString, new LogInCallback() {
                                public void done(ParseUser user, ParseException e) {
                                    if (user != null) {
                                        //WhereYouApp.addUserToCurrentInstallation(user); //Add the user to he installation object for push not. purposes
                                        //Move the user to message overview
                                        Intent i = new Intent(getApplicationContext(), MessageOverview.class);
                                        startActivity(i);
                                    } else {
                                        Toast toast = Toast.makeText(getApplicationContext(), "There was an error Signing Up",
                                                Toast.LENGTH_LONG);
                                        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_HORIZONTAL, 0, -60);
                                        toast.show();
                                    }
                                }
                            });
                        } else {
                            Toast toast = Toast.makeText(getApplicationContext(), "There was an error Signing Up",
                                    Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_HORIZONTAL, 0, -60);
                            toast.show();
                        }
                    }
                });
            } else {
                Toast toast = Toast.makeText(getApplicationContext(), "Passwords aren't the same!",
                        Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_HORIZONTAL, 0, -60);
                toast.show();


            }
        }else{
            Toast toast = Toast.makeText(getApplicationContext(), "Phone number is invalid",
                    Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_HORIZONTAL, 0, -60);
            toast.show();
        }
    }
}