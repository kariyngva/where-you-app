/**
 * @author: Hópur 7; Elsa Mjöll,Kári Yngva og Rakel Björt
 * @since: 30.10.14
 * Klasinn er listi af öllum skilaboðum milli innskráðs notanda og valins tengiliðar ("einskonar container")
 */
package is.ekr.whereyouapp;


import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class Messages {
    ArrayList<Message> messages;
    ParseUser receiver;

    public Messages(ParseUser receiver) {
        this.messages = new ArrayList<Message>();
        this.receiver = receiver;
        getMessages();
    }

    /**
     * Aðferð sem sækir skilaboð af síma og parse þjóni sem hafa átt sér stað milli innskráðs notanda og valins tengiliðar.
     **/
    public void getMessages() {
        getLocalMessages();
        getExternalMessages();
    }

    /**
     * Aðferð sem bætir skilaboðum í skilaboðalista.
     * @param list listi af skilaboðum sem skal bæta við í skilaboðalista
     **/
    public void addMessages(List<Message> list) {
        this.messages.addAll(list);
    }

    /**
     * Aðferð sem sækir skilaboð af parse þjóni sem hafa átt sér stað milli innskráðs notanda og valins tengiliðar.
     **/
    private void getExternalMessages() {
        //Query messages sent to the current user
        ParseQuery<Message> receivedMessageQuery = ParseQuery.getQuery("Message");
        receivedMessageQuery.whereEqualTo("receiver", ParseUser.getCurrentUser().getObjectId());
        receivedMessageQuery.whereEqualTo("sender", receiver);

        //Add the two queries to a list to perform and OR operation
        List queries = new ArrayList<ParseQuery>();
        queries.add(receivedMessageQuery);

        //Sort the messages in ascending order by "createdAt" field
        //ParseQuery combinedQuery = ParseQuery.or(queries).addAscendingOrder("createdAt");

        receivedMessageQuery.findInBackground(new FindCallback<Message>() {
            @Override
            public void done(List<Message> messages, ParseException e) {
                addMessages(messages); //Add the messages to the messages array-list.

                //iterate over external messages in order to save them to local datastore
                for (Message msg : messages){
                    ParseUser sender = (ParseUser) msg.get("sender");
                    Message newMsg = new Message(); //New message object so msg and newMsg are not the same object,
                                                    // otherwise we delete the local and external message.
                    newMsg.put("localCreatedAt", msg.getCreatedAt());
                    newMsg.put("receiver", msg.getString("receiver"));
                    newMsg.put("sender", sender);
                    newMsg.put("MessageBody", msg.getMessageBody());
                    newMsg.pinInBackground(); //save the message to our local datastore
                    msg.deleteInBackground(); //delete the message from the parse server
                }
            }
        });
    }

    /**
     * Aðferð sem sækir skilaboð á síma sem hafa átt sér stað milli innskráðs notanda og valins tengiliðar.
     **/
    private void getLocalMessages() {
        //Query messages sent by the current user
        ParseQuery<Message> sentMessageQuery = ParseQuery.getQuery("Message");
        sentMessageQuery.fromLocalDatastore();
        sentMessageQuery.whereEqualTo("sender", ParseUser.getCurrentUser());
        sentMessageQuery.whereEqualTo("receiver", receiver.getObjectId());

        //Query messages sent to the current user
        ParseQuery<Message> receivedMessageQuery = ParseQuery.getQuery("Message");
        receivedMessageQuery.fromLocalDatastore();
        receivedMessageQuery.whereEqualTo("receiver", ParseUser.getCurrentUser().getObjectId());
        receivedMessageQuery.whereEqualTo("sender", receiver);

        //Add the two queries to a list to perform and OR operation
        List queries = new ArrayList<ParseQuery>();
        queries.add(sentMessageQuery);
        queries.add(receivedMessageQuery);

        //Sort the messages in ascending order by "createdAt" field
        ParseQuery combinedQuery = ParseQuery.or(queries).addAscendingOrder("localCreatedAt");
        combinedQuery.fromLocalDatastore();

        try {
            this.messages.addAll(combinedQuery.find());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Aðferð sem skilar lista af skilaboðum milli innskráðs notanda og valins tengiliðar.
     * @return skilar lista af skilaboðum
    **/
    public List<Message> getMessageList()
    {
        return this.messages;
    }

    /**
     * Skilar fjölda skilaboða í skilaboðalista.
     * @return fjöldi skilaboða
     */
    public int getMessageListSize() { return this.messages.size(); }

    /**
     * Aðferð sem býr til nýjan tóman skilaboða hlut, fyllur út í móttakanda og texta skilaboðanna og bætir í lista yfir skilaboð.
     * @param messageBody strengurinn sem eru skilaboðin sem skal senda
     **/
    public Message sendMessage(String messageBody) {
        Message msg = new Message();
        this.messages.add(msg);
        msg.prepareMessage(this.receiver, messageBody);
        return msg;
    }
}