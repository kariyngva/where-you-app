/**
 * @author: Hópur 7; Elsa Mjöll,Kári Yngva og Rakel Björt
 * @since: 12.11.14
 * Klasi sem heldur utan um talblöðrur skilaboða
 */

package is.ekr.whereyouapp;

public class ChatBubble {
    public boolean left;
    public String message;

    public ChatBubble(boolean left, String message) {
        super();
        this.left = left;
        this.message = message;
    }
}