/**
 * @author: Hópur 7; Elsa Mjöll,Kári Yngva og Rakel Björt
 * @since: 30.10.14
 *
 * Klasinn inniheldur skilaboðahlut, og vistar skilaboð á vefþjóni þ.e sendanda og móttakanda.
 */
package is.ekr.whereyouapp;

import com.parse.ParseACL;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Date;

@ParseClassName("Message")
public class Message extends ParseObject {
    public Message() {}

    /**
     * Aðferð sem fyllir út tómann skilaboðahlut með móttakanda og texta.
     * @param receiver móttakandi skilaboðanna
     * @param messageBody strengur sem inniheldur skilaboðin sem skal senda
     **/
    public void prepareMessage(ParseUser receiver, String messageBody) {
        ParseACL messageACL = new ParseACL();
        Date localCreatedAt = new Date();

        messageACL.setReadAccess(ParseUser.getCurrentUser(), true);
        messageACL.setReadAccess(receiver, true);
        messageACL.setWriteAccess(receiver, true);

        put("sender", ParseUser.getCurrentUser());
        put("receiver", receiver.getObjectId());
        put("MessageBody", messageBody);
        put("localCreatedAt", localCreatedAt);

        setACL(messageACL);
        sendMessage(receiver);
    }

    /**
     * Aðferð sem vistar skilaboð (þ.e. þennan hlut) á parse þjóni.
     * @param receiver er sá notandi sem á að fá skilaboðin
     **/
    public void sendMessage(ParseUser receiver) {
        this.pinInBackground();
        this.saveInBackground( new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null)
                {
                    System.out.println("gat ekki sent skilaboð");
                    e.printStackTrace(System.out);
                    //TODO: Toast error
                }
                else{
                    System.out.println("gat sent skilaboð");

                    //Gerum Installation fyrirspurn fyrir þann notanda sem er verið að senda skilaboð til
                    //ParseQuery pushQuery = ParseInstallation.getQuery();
                    //pushQuery.whereEqualTo("username", receiver.getUsername());

                    //Sendum push notification á notandan sem pushQuery mun skila
                    /*ParsePush push = new ParsePush();
                    push.setQuery(pushQuery); // Set our Installation query
                    push.setMessage("You have a new message from " + WhereYouApp.currentUser.getUsername());
                    push.sendInBackground();*/
                }
            }
        });
    }

    /**
     * Aðferð sem segir hvort að sendandi þessa skilaboða-hlutar sé sá sami og innskráður notandi.
     * @return true ef sendandi skilaboðanna er sá sami og innskráður notandi, false annars
     */
    public boolean senderIsCurrentUser() {
        return this.get("sender").equals(ParseUser.getCurrentUser());
    }

    /**
     * Aðferð sem skilar texta skilaboðanna.
     * @return strengur sem inniheldur texta skilaboðanna
     */
    public String getMessageBody() {
        return this.getString("MessageBody");
    }
}