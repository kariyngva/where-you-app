/**
 * @author: Hópur 7; Elsa Mjöll,Kári Yngva og Rakel Björt
 * @since: 13.10.14
 * Klasinn sem birtir Upphafsskjá í forritinu, sem inniheldur tvo hnappa.
 * Annar til að nýskrá notanda, og hinn skráir notanda inn.
 */
package is.ekr.whereyouapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.parse.Parse;
import com.parse.ParseUser;


public class MainActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Switch to message overview if a user is logged in
        if (ParseUser.getCurrentUser() != null)
        {
            Intent i = new Intent( getApplicationContext(), MessageOverview.class );
            startActivity(i);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_sign_out) {

            return true;
        }


        return super.onOptionsItemSelected(item);
    }


    /**
     * Aðferð til að skipta yfir á nýskráningarskjá
     **/
    public void onClickSignUp(View view) {
        Intent i = new Intent(getApplicationContext(), SignUp.class);
        startActivity(i);
    }

    /**
     * Aðferð til að skipta yfir á innskráningarskjá
     **/
    public void onClickLogIn(View view) {
        Intent i = new Intent(getApplicationContext(), LogIn.class);
        startActivity(i);
    }

}