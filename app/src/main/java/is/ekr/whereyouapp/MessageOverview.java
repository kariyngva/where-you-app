/**
 * @author: Hópur 7; Elsa Mjöll,Kári Yngva og Rakel Björt
 * @since: 27.10.14
 * Klasinn sem birtir MyContacts(tengiliðar) hnappinn og leitarstrenginn til að leita eftir tengilið.
 */
package is.ekr.whereyouapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.ParseUser;

import java.util.List;


public class MessageOverview extends Activity {
    TextView searchTerm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_overview);

        //Tökum við leitarstreng og vistum hann sem "extra" og færum notandann yfir í SearchResult activity
        searchTerm = (TextView) findViewById(R.id.action_search);
        searchTerm.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                String searchTermString = searchTerm.getText().toString();

                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Intent i = new Intent( getApplicationContext(), SearchResult.class );
                    i.putExtra("searchTerm", searchTermString);
                    startActivity(i);
                    handled = true;
                }
                return handled;
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.message_overview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_sign_out) {
            if(WhereYouApp.logout())
            {
                Intent i = new Intent(getApplicationContext(), LogIn.class);
                startActivity(i);
                Toast toast = Toast.makeText(getApplicationContext(), "You have Logged Out!",
                Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_HORIZONTAL, 0, -60);
                toast.show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Aðferð til að skipta yfir á My contacts skjá
     **/
    public void onClickLogIn(View view) {
        Intent i = new Intent(getApplicationContext(), Contacts.class);
        startActivity(i);
    }
}