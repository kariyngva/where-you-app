package is.ekr.whereyouapp;

/**
 * @author: Hópur 7; Elsa Mjöll,Kári Yngva og Rakel Björt
 * @since: 21.11.14
 * Prófunarklasi fyrir stærð skilaboðalisa, inn -og útskráningu.
 */

import android.test.ActivityInstrumentationTestCase2;
import com.parse.ParseException;
import com.parse.ParseUser;

public class ApplicationUnitTest
        extends ActivityInstrumentationTestCase2<MessageOverview> {

    private MessageOverview messageOverviewActivity;
    private Messages messages;
    private ParseUser receiver;

    public ApplicationUnitTest() {
        super(MessageOverview.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        messageOverviewActivity = getActivity();
    }

    /**
     * Aðferð sem reynir að skrá inn notanda "Gdog", og athugar hvort til sé samtal milli "Gdog" og "rakelbjort".
     */
    public void testMessageList() {

        try {
            ParseUser.logIn("Gdog", "a");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        receiver = WhereYouApp.getContactByName("rakelbjort");
        messages = new Messages(this.receiver);

        assertTrue(this.messages.getMessageListSize() > 0);
    }

    /**
     * Aðferð sem reynir að skrá inn notanda.
     */
    public void testLogin() {
        try {
            ParseUser.logIn("Gdog", "a");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        assertNotNull( ParseUser.getCurrentUser() );
    }

    /**
     * Aðferð sem reynir að skrá inn notanda og skrá svo út notanda
     */
    public void testLogout() {
        try {
            ParseUser.logIn("Gdog", "a");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        WhereYouApp.logout();

        assertNull(ParseUser.getCurrentUser());
    }
}
